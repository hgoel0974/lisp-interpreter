using System.Collections;
using System.Collections.Generic;

namespace Cardinal2Lang.Interpreter
{
    public abstract class LispFunc : LispType
    {
        public abstract LispType Evaluate(LispType t, LispEnvironment env);
    }
}