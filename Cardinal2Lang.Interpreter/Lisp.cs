using System.ComponentModel;
using System;
using System.Collections.Generic;

namespace Cardinal2Lang.Interpreter
{
    public enum TokenType
    {
        Unknown,

        //List definition
        OpenParen,
        CloseParen,

        //Shorthands
        Quote,
        Quasiquote,
        Unquote,

        //Constants
        String,
        Symbol,

        //Comments
        LineComment,
    }
    public struct Token
    {
        public TokenType Type;
        public int Column;
        public int Line;
        public string Value;

        public override string ToString()
        {
            return $"{Line}:{Column} [{Type}] {Value}";
        }
    }

    public class LispEnvironment
    {
        private readonly Dictionary<string, LispFunc> variables;

        public LispEnvironment()
        {
            variables = new Dictionary<string, LispFunc>
            {
                ["+"] = new LispMathOp(LispMathOpNames.Add),
                ["-"] = new LispMathOp(LispMathOpNames.Subtract),
                ["*"] = new LispMathOp(LispMathOpNames.Multiply),
                ["/"] = new LispMathOp(LispMathOpNames.Divide)
            };
        }

        public LispEnvironment(IDictionary<string, LispFunc> vars)
        {
            variables = new Dictionary<string, LispFunc>(vars);
        }

        public LispEnvironment Fork()
        {
            return new LispEnvironment(variables);
        }

        public LispFunc Get(string name)
        {
            if (variables.ContainsKey(name))
                return variables[name];
            else
                throw new Exception("Undefined variable.");
        }
    }

    public class Lisp
    {
        private Token[] tokens;
        private Queue<Token> tokenQ;
        private LispEnvironment environment;

        public Lisp()
        {
            environment = new LispEnvironment();
        }

        public Token[] Tokenize(string code)
        {
            var chars = code.ToCharArray();
            List<Token> tokens = new List<Token>();

            bool inLineComment = false;
            bool inString = false;
            bool escapedChar = false;

            int startLineIdx = 0;
            int startColIdx = 0;

            int lineIdx = 0;
            int colIdx = 0;

            TokenType token_type = TokenType.Symbol;
            string token_val = "";

            void terminate_token()
            {
                if (!string.IsNullOrEmpty(token_val))
                {
                    add_token(token_type, token_val, startLineIdx, startColIdx);
                }
                token_type = TokenType.Symbol;
                token_val = "";
            }

            void add_token(TokenType type, string val, int line, int col)
            {
                tokens.Add(new Token()
                {
                    Type = type,
                    Value = val,
                    Line = line,
                    Column = col,
                });
            }

            for (int charIdx = 0; charIdx < chars.Length; charIdx++)
            {
                char curChar = chars[charIdx];

                switch (curChar)
                {
                    case '\n':
                        if (inLineComment)
                        {
                            terminate_token();
                            inLineComment = false;
                        }
                        lineIdx++;
                        colIdx = 0;
                        break;
                    case ';':
                        if (!inString)
                        {
                            terminate_token();

                            startLineIdx = lineIdx;
                            startColIdx = colIdx;

                            inLineComment = true;
                            token_type = TokenType.LineComment;
                        }
                        break;
                    case '"':
                        if (!inString && !escapedChar)
                        {
                            startLineIdx = lineIdx;
                            startColIdx = colIdx;

                            //Opening string
                            inString = true;
                            token_type = TokenType.String;
                            token_val = "";
                        }
                        else if (inString && !escapedChar)
                        {
                            //Closing string
                            terminate_token();
                            inString = false;
                        }
                        else if (inString && escapedChar)
                        {
                            token_val += "\"";
                            escapedChar = false;
                        }
                        break;
                    case '\\':
                        if (inString && !escapedChar)
                        {
                            escapedChar = true; //if this isn't escaped itself, it enables the escape character
                        }
                        else if (inString && escapedChar)
                        {
                            token_val += "\\";
                        }
                        break;
                    case '(':
                        if (!inString && !escapedChar && !inLineComment)
                        {
                            terminate_token();
                            startLineIdx = lineIdx;
                            startColIdx = colIdx + 1;
                            add_token(TokenType.OpenParen, "(", lineIdx, colIdx);
                        }
                        else if (inString)
                        {
                            if (escapedChar) throw new Exception("Unknown escape sequence.");
                            token_val += curChar;
                        }
                        break;
                    case ')':
                        if (!inString && !escapedChar && !inLineComment)
                        {
                            terminate_token();
                            startLineIdx = lineIdx;
                            startColIdx = colIdx + 1;
                            add_token(TokenType.CloseParen, ")", lineIdx, colIdx);
                        }
                        else if (inString)
                        {
                            if (escapedChar) throw new Exception("Unknown escape sequence.");
                            token_val += curChar;
                        }
                        break;
                    case '\'':
                        if (!inString && !escapedChar && !inLineComment)
                        {
                            terminate_token();
                            startLineIdx = lineIdx;
                            startColIdx = colIdx + 1;
                            add_token(TokenType.Quote, "'", lineIdx, colIdx);
                        }
                        else if (inString)
                        {
                            if (escapedChar) throw new Exception("Unknown escape sequence.");
                            token_val += curChar;
                        }
                        break;
                    case '`':
                        if (!inString && !escapedChar && !inLineComment)
                        {
                            terminate_token();
                            startLineIdx = lineIdx;
                            startColIdx = colIdx + 1;
                            add_token(TokenType.Quasiquote, "`", lineIdx, colIdx);
                        }
                        else if (inString)
                        {
                            if (escapedChar) throw new Exception("Unknown escape sequence.");
                            token_val += curChar;
                        }
                        break;
                    case ',':
                        if (!inString && !escapedChar && !inLineComment)
                        {
                            terminate_token();
                            startLineIdx = lineIdx;
                            startColIdx = colIdx + 1;
                            add_token(TokenType.Unquote, ",", lineIdx, colIdx);
                        }
                        else if (inString)
                        {
                            if (escapedChar) throw new Exception("Unknown escape sequence.");
                            token_val += curChar;
                        }
                        break;
                    case ' ':
                    case '\t':
                        if (!inString && !inLineComment)
                        {
                            terminate_token();
                        }
                        else if (inString)
                        {
                            //Append to the token value
                            if (escapedChar) throw new Exception("Unknown escape sequence.");
                            token_val += curChar;
                        }
                        break;
                    default:
                        if (escapedChar)
                        {
                            token_val += curChar switch
                            {
                                't' => "\t",
                                'n' => "\n",
                                _ => throw new Exception("Unknown escape sequence."),
                            };
                            escapedChar = false;
                        }
                        else
                        {
                            token_val += curChar;
                        }
                        break;
                }

                colIdx++;
            }
            terminate_token();

            this.tokens = tokens.ToArray();
            tokenQ = new Queue<Token>();
            for (int i = 0; i < tokens.Count; i++)
                if (tokens[i].Type != TokenType.LineComment)
                    tokenQ.Enqueue(tokens[i]);
            return this.tokens;
        }

        public void CheckTokens()
        {
            //Ensure all parens, brackets and braces have a corresponding pair
            Stack<char> pairs = new Stack<char>();

            for (int i = 0; i < tokens.Length; i++)
            {
                if (tokens[i].Type == TokenType.OpenParen)
                    pairs.Push('(');
                else if (tokens[i].Type == TokenType.CloseParen)
                {
                    if (pairs.Count == 0 || pairs.Pop() != '(')
                        throw new Exception("Pairs don't match.");
                }
            }
            if (pairs.Count != 0)
                throw new Exception("Pairs don't match.");
        }

        private LispList ReadList()
        {
            var l = new LispList();

            tokenQ.Dequeue();   //OpenParen
            while (tokenQ.Peek().Type != TokenType.CloseParen)
            {
                l.Add(Eval());
            }
            tokenQ.Dequeue(); //CloseParen

            return l;
        }

        private LispType ReadAtom()
        {
            var tkn = tokenQ.Dequeue(); //Symbol or other definition
            if (tkn.Type == TokenType.String)
            {
                return new LispAtom(tkn.Value, AtomType.String);
            }
            else if (tkn.Type == TokenType.Symbol)
            {
                //these can only be evaluated and verified at interpretation time
                return new LispAtom(tkn.Value, AtomType.Symbol);
            }
            else if (tkn.Type == TokenType.Quote)
            {
                return new LispAtom(Eval(), AtomType.Quote);
            }
            else if (tkn.Type == TokenType.Quasiquote)
            {
                return new LispAtom(Eval(), AtomType.Quasiquote);
            }
            else if (tkn.Type == TokenType.Unquote)
            {
                return new LispAtom(Eval(), AtomType.Unquote);
            }
            else throw new Exception("Unrecognized atom.");
        }

        public LispType Eval()
        {
            try
            {
                //Evaluate recursively until first mismatched paren or end of token stream
                if (tokenQ.Peek().Type == TokenType.OpenParen)
                {
                    //Evaluate recursively
                    return ReadList();
                }
                else
                {
                    return ReadAtom();
                }
            }
            catch (InvalidOperationException)
            {
                return new LispList();
            }
        }

        public LispType Apply(LispType a, LispEnvironment env, bool inQuasiquote)
        {
            if (a is LispList)
            {
                var l = a as LispList;
                if (l.Count == 0)
                    return l;

                //Process second member onwards
                var parameters = new LispList();

                var f = Apply(l[0], env, inQuasiquote);
                parameters.Add(f);
                for (int i = 1; i < l.Count; i++)
                {
                    parameters.Add(Apply(l[i], env, inQuasiquote));
                }
                //Lookup the lisp function
                //Apply first function with the given arguments
                if (inQuasiquote)
                    return parameters;
                else
                    return (f as LispFunc).Evaluate(parameters, env);
            }
            else if (a is LispAtom)
            {
                //Evaluate the atom
                var l = a as LispAtom;

                if (l.Type == AtomType.Symbol)
                {
                    if ((string)l.Value == "true")
                    {
                        return new LispAtom("true", AtomType.True);
                    }
                    //else if (byte.TryParse((string)l.Value, out var v_b))
                    //{
                    //    return new LispAtom(v_b);
                    //}
                    //else if (sbyte.TryParse((string)l.Value, out var v_sb))
                    //{
                    //    return new LispAtom(v_sb);
                    //}
                    //else if (short.TryParse((string)l.Value, out var v_s))
                    //{
                    //    return new LispAtom(v_s);
                    //}
                    //else if (ushort.TryParse((string)l.Value, out var v_us))
                    //{
                    //    return new LispAtom(v_us);
                    //}
                    //else if (int.TryParse((string)l.Value, out var v_i))
                    //{
                    //    return new LispAtom(v_i);
                    //}
                    //else if (uint.TryParse((string)l.Value, out var v_ui))
                    //{
                    //    return new LispAtom(v_ui);
                    //}
                    else if (ulong.TryParse((string)l.Value, out var v_ul))
                    {
                        return new LispAtom(v_ul);
                    }
                    else if (long.TryParse((string)l.Value, out var v_l))
                    {
                        return new LispAtom(v_l);
                    }
                    //else if (float.TryParse((string)l.Value, out var v_f))
                    //{
                    //    return new LispAtom(v_f);
                    //}
                    else if (double.TryParse((string)l.Value, out var v_d))
                    {
                        return new LispAtom(v_d);
                    }

                    //The environment is a map
                    //Read the variable from the environment
                    return env.Get((string)l.Value);
                }
                else if (l.Type == AtomType.String)
                {
                    return l;
                }
                else if (l.Type == AtomType.Quote)
                {
                    return l.Value as LispType;
                }
                else if (l.Type == AtomType.Quasiquote)
                {
                    return Apply(l.Value as LispType, env, true);
                }
                else if (l.Type == AtomType.Unquote)
                {
                    if (!inQuasiquote)
                        throw new NotImplementedException("Unquotes are only allowed in quasiquotes.");
                    else
                        return Apply(l.Value as LispType, env, false);  //Evaluate unquoted expression as normal
                }
                else
                    throw new Exception("Unknown type.");
            }
            else
                throw new Exception("Unknown type.");
        }

        public LispType Interpret()
        {
            //Evaluate the token stream
            var ast = Eval();

            //Process the tree depth first
            return Apply(ast, environment, false);
        }
    }
}
