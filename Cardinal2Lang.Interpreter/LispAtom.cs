namespace Cardinal2Lang.Interpreter
{
    public enum AtomType
    {
        Symbol,
        String,
        Quote,
        Quasiquote,
        True,
        Number,
        Unquote,
    }

    public class LispAtom : LispType
    {
        public object Value;
        public AtomType Type;

        public LispAtom(string s, AtomType t)
        {
            Value = s;
            Type = t;
        }

        public LispAtom(LispType v, AtomType t)
        {
            Value = v;
            Type = t;
        }

        public LispAtom(byte v)
        {
            Type = AtomType.Number;
            Value = v;
        }

        public LispAtom(sbyte v)
        {
            Type = AtomType.Number;
            Value = v;
        }

        public LispAtom(short v)
        {
            Type = AtomType.Number;
            Value = v;
        }

        public LispAtom(ushort v)
        {
            Type = AtomType.Number;
            Value = v;
        }

        public LispAtom(int v)
        {
            Type = AtomType.Number;
            Value = v;
        }

        public LispAtom(uint v)
        {
            Type = AtomType.Number;
            Value = v;
        }

        public LispAtom(long v)
        {
            Type = AtomType.Number;
            Value = v;
        }

        public LispAtom(ulong v)
        {
            Type = AtomType.Number;
            Value = v;
        }

        public LispAtom(float v)
        {
            Type = AtomType.Number;
            Value = v;
        }

        public LispAtom(double v)
        {
            Type = AtomType.Number;
            Value = v;
        }

        public override string ToString()
        {
            return $"{Value}";
        }
    }
}