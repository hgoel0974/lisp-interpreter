﻿using System;

namespace Cardinal2Lang.Interpreter
{
    class Program
    {
        static void Main(string[] args)
        {
            //LISP REPL provides enough infrastructure for a compiler to be written in the LISP
            //the compiler just generates bytecode
            //compiler can then compile itself into bytecode for the OS

            var lisp = new Lisp();

            bool exit = false;
            while (!exit)
            {
                Console.Write("shell>");
                var code = Console.ReadLine();

                var result = lisp.Tokenize(code);
                lisp.CheckTokens();

                //var evalType = lisp.Eval();
                Console.WriteLine(lisp.Interpret());

                //foreach (Token t in result)
                //    Console.WriteLine(t);

                //Console.WriteLine(evalType);

                //exit = true;
            }
        }
    }
}
