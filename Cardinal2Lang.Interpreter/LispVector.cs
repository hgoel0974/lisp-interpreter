using System;
using System.Collections;
using System.Collections.Generic;

namespace Cardinal2Lang.Interpreter
{
    public class LispVector : LispType, IEnumerable, IList<LispType>
    {
        private readonly List<LispType> Members;

        public LispVector()
        {
            Members = new List<LispType>();
        }

        public LispType this[int index] { get => ((IList<LispType>)Members)[index]; set => ((IList<LispType>)Members)[index] = value; }

        public int Count => ((ICollection<LispType>)Members).Count;

        public bool IsReadOnly => ((ICollection<LispType>)Members).IsReadOnly;

        public override string ToString(int lv)
        {
            string s = "";
            for (int i = 0; i < lv; i++)
                s += "\t";
            s += "VECTOR:\n";

            for (int i = 0; i < Members.Count; i++)
                s += Members[i].ToString(lv + 1) + "\n";
            return s;
        }

        public override string ToString() => ToString(0);

        public void Add(LispType t)
        {
            Members.Add(t);
        }

        public void Clear()
        {
            ((ICollection<LispType>)Members).Clear();
        }

        public bool Contains(LispType item)
        {
            return ((ICollection<LispType>)Members).Contains(item);
        }

        public void CopyTo(LispType[] array, int arrayIndex)
        {
            ((ICollection<LispType>)Members).CopyTo(array, arrayIndex);
        }

        public IEnumerator<LispType> GetEnumerator()
        {
            return ((IEnumerable<LispType>)Members).GetEnumerator();
        }

        public int IndexOf(LispType item)
        {
            return ((IList<LispType>)Members).IndexOf(item);
        }

        public void Insert(int index, LispType item)
        {
            ((IList<LispType>)Members).Insert(index, item);
        }

        public bool Remove(LispType item)
        {
            return ((ICollection<LispType>)Members).Remove(item);
        }

        public void RemoveAt(int index)
        {
            ((IList<LispType>)Members).RemoveAt(index);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable)Members).GetEnumerator();
        }
    }
}
