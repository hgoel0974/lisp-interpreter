using System;

namespace Cardinal2Lang.Interpreter
{
    public enum LispMathOpNames
    {
        Add,
        Subtract,
        Multiply,
        Divide
    }

    public class LispMathOp : LispFunc
    {
        private readonly LispMathOpNames opName;
        public LispMathOp(LispMathOpNames op)
        {
            opName = op;
        }

        public override LispType Evaluate(LispType t, LispEnvironment env)
        {
            switch (opName)
            {
                case LispMathOpNames.Add:
                    {
                        var l = t as LispList;
                        var o = (ulong)(l[1] as LispAtom).Value;
                        for (int i = 2; i < l.Count; i++)
                            o += (ulong)(l[i] as LispAtom).Value;
                        return new LispAtom(o);
                    }
                case LispMathOpNames.Subtract:
                    {
                        var l = t as LispList;
                        var o = (ulong)(l[1] as LispAtom).Value;
                        for (int i = 2; i < l.Count; i++)
                            o -= (ulong)(l[i] as LispAtom).Value;
                        return new LispAtom(o);
                    }
                case LispMathOpNames.Multiply:
                    {
                        var l = t as LispList;
                        var o = (ulong)(l[1] as LispAtom).Value;
                        for (int i = 2; i < l.Count; i++)
                            o *= (ulong)(l[i] as LispAtom).Value;
                        return new LispAtom(o);
                    }
                case LispMathOpNames.Divide:
                    {
                        var l = t as LispList;
                        var o = (ulong)(l[1] as LispAtom).Value;
                        for (int i = 2; i < l.Count; i++)
                            o /= (ulong)(l[i] as LispAtom).Value;
                        return new LispAtom(o);
                    }
                default:
                    throw new Exception("Unknown operation.");
            }
        }

        public override string ToString()
        {
            return opName switch
            {
                LispMathOpNames.Add => "+",
                LispMathOpNames.Subtract => "-",
                LispMathOpNames.Multiply => "*",
                LispMathOpNames.Divide => "/",
                _ => throw new NotImplementedException("Operation not implemented.")
            };
        }
    }
}